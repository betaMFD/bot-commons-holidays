import { SlashCommandBuilder } from 'discord.js';

const holidays = {
  'new year': { date: '01/01/', name: 'the New Year', greeting: 'Happy New Year!', greetingEve: 'Happy New Year!' },
  halloween: { date: '10/31/', name: 'Halloween', greeting: 'Happy Halloween!', greetingEve: 'Happy Halloween!' },
  christmas: { date: '12/25/', name: 'Christmas', greeting: 'Merry Christmas!', greetingEve: 'Happy Christmas Eve!' }
};

export const data = new SlashCommandBuilder()
  .setName('holiday')
  .setDescription('Counts days until the selected holiday')
  .addStringOption(option =>
    option.setName('name')
      .setDescription('Select the holiday')
      .setRequired(true)
      .addChoices(
        { name: 'New Year', value: 'new year' },
        { name: 'Halloween', value: 'halloween' },
        { name: 'Christmas', value: 'christmas' }
      ));

export async function execute(interaction) {
  const holidayKey = interaction.options.getString('name');
  const today = new Date();
  const holiday = holidays[holidayKey];
  const goalDate = new Date(holiday.date + (today.getFullYear()));

  if (goalDate < today) {
    goalDate.setFullYear(today.getFullYear() + 1);
  }

  const timeUntilGoalDate = goalDate - today;
  const daysUntilGoalDate = timeUntilGoalDate / (1000 * 3600 * 24);
  const fullDays = Math.ceil(daysUntilGoalDate);

  let response;
  if (fullDays === 1) {
    response = `There is only one day left until ${holiday.name}! ${holiday.greetingEve}`;
  } else if (fullDays === 0) {
    response = `It's ${holiday.name} today! ${holiday.greeting}`;
  } else {
    response = `There are ${fullDays} days until ${holiday.name}!`;
  }

  await interaction.reply(response);
}
